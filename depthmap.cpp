#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/ximgproc/disparity_filter.hpp>
#include <iostream>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "TouchPlus.h"

#define MJPEG 1
#define UNCOMPRESSED 0

#define WIDTH 1280
#define HEIGHT 480
#define FRAMERATE 60
#define COMPRESSION MJPEG

using namespace cv;
using namespace cv::ximgproc;
using namespace std;

Mat image(HEIGHT, WIDTH, CV_8UC3, Scalar(0, 0, 0));
Mat leftframe(480, 640, CV_8UC3, Scalar(0, 0, 0));
Mat rightframe(480, 640, CV_8UC3, Scalar(0, 0, 0));
Mat leftcorrected(480, 640, CV_8UC3, Scalar(0, 0, 0));
Mat rightcorrected(480, 640, CV_8UC3, Scalar(0, 0, 0));
Mat T;  // translation vector between the camera lenses
Mat R;  // rotation matrix between the camera lenses

bool leds_on = false;
bool use_ae = false;
bool use_awb = false;

volatile bool continue_processing = true;

int picturenumber = 0;

float leftdistcoeffsnum[5] = {-4.548508e+02, 0.000000e+00, 9.541841e-04, -2.214657e-07, 1.997202e-09};
float rightdistcoeffsnum[5] = {-4.508328e+02, 0.000000e+00, 8.753889e-04, 5.085536e-07, 7.267991e-10};
float leftmatrixnum[9] = {7.2714497559343192e+02, 0., 3.1950000000000000e+02, 0., 7.2714497559343192e+02, 2.3950000000000000e+02, 0., 0., 1.};
float rightmatrixnum[9] = {4.7271719264279130e+02, 0., 3.1950000000000000e+02, 0., 4.7271719264279130e+02, 2.3950000000000000e+02, 0., 0., 1};
Mat leftmatrix = cv::Mat(3, 3, CV_32F, leftmatrixnum);
Mat rightmatrix = cv::Mat(3, 3, CV_32F, rightmatrixnum);
Mat leftdistcoeffs = cv::Mat(5, 1, CV_32F, leftdistcoeffsnum);
Mat rightdistcoeffs = cv::Mat(5, 1, CV_32F, rightdistcoeffsnum);

unsigned char *buffer;
int decompressJPEG = 0;
uvc_frame_t *bgr;
unsigned char * liveData;
volatile long currentFrame = 0;

void takepicture(){

  //picturenumber++;
  //char photo [1];
  //photo = picturenumber;
  //char leftfile [11];
  //char rightfile [11];

  //strcpy(leftfile, "VID5/");
  //strcpy(rightfile, "VID5/");

  //strcat(leftfile, photo);
  //strcat(rightfile, photo);

  //strcat(leftfile, "l.jpg");
  //strcat(rightfile, "r.jpg");

  //cout << leftfile;
  //cout << rightfile;

  imwrite( "VID5/left/xx1.jpg", leftframe );
  imwrite( "VID5/right/xx1.jpg", rightframe );

}

void process_key_switches(char key){

  if (key == 27) continue_processing = false;
  if (key == 's'){
    takepicture();
  }
}

void cb(uvc_frame_t *frame, void *ptr) {

  cout << "Entered frame loop!";

  uvc_error_t ret;

  if (COMPRESSION == UNCOMPRESSED){
    ret = uvc_any2bgr(frame, bgr);
  }
  else {
    ret = uvc_mjpeg2rgb(frame, bgr);

  if (ret) {
    uvc_perror(ret, "change to BGR error");
    uvc_free_frame(bgr);
    return;
  }

    image.data = (uchar*)bgr->data;
    flip(image,image,0);

    image.colRange(0,640).copyTo(leftframe);
    image.colRange(640,1280).copyTo(rightframe);

    // capture both images
    // run stereorectify to produce R1 and R2
    // apply initundistortrectifymap to alter images
    // use remap to output the corrected images

    undistort(leftframe, leftcorrected, leftmatrix, leftdistcoeffs);
    undistort(rightframe, rightcorrected, rightmatrix, rightdistcoeffs);

    imshow("Left Image", leftframe);
    imshow("Right Image", rightframe);

    imshow("Corrected Left", leftcorrected);
    imshow("Corrected Right", rightcorrected);

  }
}

int main(int ac, char** av)
{

    bgr = uvc_allocate_frame(WIDTH*HEIGHT*3);

    init_camera();

    int retval = startVideoStream(WIDTH,HEIGHT,60,MJPEG,cb);
    if (retval == -1){
      return -1;
    }
    turnLEDsOff();
    int x,y,z;
    getAccelerometerValues(&x,&y,&z);
    fprintf(stderr,"x= %d y= %d z= %d\n",x,y,z);

    namedWindow("Left Image",1);
    namedWindow("Right Image",1);

    while(continue_processing){
      //Ptr<StereoSGBM> left_matcher  = StereoSGBM::create(0,max_disp,wsize);
      //left_matcher->setP1(24*wsize*wsize);
      //left_matcher->setP2(96*wsize*wsize);
      //left_matcher->setPreFilterCap(63);
      //left_matcher->setMode(StereoSGBM::MODE_SGBM_3WAY);
      //wls_filter = createDisparityWLSFilter(left_matcher);
      //Ptr<StereoMatcher> right_matcher = createRightMatcher(left_matcher);

      //matching_time = (double)getTickCount();
      //left_matcher-> compute(left_for_matcher, right_for_matcher,left_disp);
      //right_matcher->compute(right_for_matcher,left_for_matcher, right_disp);
      //matching_time = ((double)getTickCount() - matching_time)/getTickFrequency();

      char key = waitKey(10);
        if (key>0)process_key_switches(key);
      }
    destroyWindow("Left Image");
    destroyWindow("Right Image");
    stop_VideoStream();

}
