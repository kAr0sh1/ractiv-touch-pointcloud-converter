#include <iostream>

#include "depthmap_pcl.hpp"

using namespace std;
using namespace cv;
using namespace cv::ximgproc;
using namespace pcl;

bool continue_processing = true;
bool no_distort = true;
bool display_confmap = true;
bool display_disparity = true;
bool display_pc = true;
bool video_filter = false;
bool show_raw_video = false;
bool autoWB = true;
bool debug = false;
bool runonce = false;

float leftdistcoeffsnum[5] = {-2.1369926841385427e+00, 1.2192113031006272e+01, 0., 0., -4.9785055568110550e+01};
float rightdistcoeffsnum[5] = {-4.3915698123537433e-01, 2.4496566763909952e-01, 0., 0., -9.8114125917790132e-02};
float leftmatrixnum[9] = {9.5600542664488717e+02, 0., 3.1950000000000000e+02, 0., 9.5600542664488717e+02, 2.3950000000000000e+02, 0., 0., 1.};
float rightmatrixnum[9] = {4.7271719264279130e+02, 0., 3.1950000000000000e+02, 0., 4.7271719264279130e+02, 2.3950000000000000e+02, 0., 0., 1.};
float leftdistnum[9] = {9.5600542664488717e+02, 0., 3.1950000000000000e+02, 0., 9.5600542664488717e+02, 2.7550000000000000e+02, 0., 0., 1.};
float rightdistnum[9] = {4.7271719264279130e+02, 0., 3.1950000000000000e+02, 0., 4.7271719264279130e+02, 2.3950000000000000e+02, 0., 0., 1.};
Mat leftdistmatrix = cv::Mat(3, 3, CV_32F, leftdistnum);
Mat rightdistmatrix = cv::Mat(3, 3, CV_32F, rightdistnum);
Mat leftmatrix = cv::Mat(3, 3, CV_32F, leftmatrixnum);
Mat rightmatrix = cv::Mat(3, 3, CV_32F, rightmatrixnum);
Mat leftdistcoeffs = cv::Mat(5, 1, CV_32F, leftdistcoeffsnum);
Mat rightdistcoeffs = cv::Mat(5, 1, CV_32F, rightdistcoeffsnum);

Mat image(480, 1280, CV_8UC3, Scalar(0, 0, 0));
Mat leftframe(480, 640, CV_8UC3, Scalar(0, 0, 0));
Mat rightframe(480, 640, CV_8UC3, Scalar(0, 0, 0));
Mat leftcorrected(480, 640, CV_8UC3, Scalar(0, 0, 0));
Mat rightcorrected(480, 640, CV_8UC3, Scalar(0, 0, 0));
Mat pcl_overlay(480, 640, CV_32FC3, Scalar(0, 0, 0));
Mat point_cloud_vals(480, 640, CV_32F, Scalar(0, 0, 0));


int filenum = 0;

int numDisparities = 32;
int blockSize = 12;
int filtercap = 63;

int P1 = 24*blockSize*blockSize;
int P2 = 96*blockSize*blockSize;
int dispmaxdiff = 50;
int speckleWS = 0; // 400
int speckleR = 0; // 200

int matchmode = StereoSGBM::MODE_SGBM_3WAY;
Ptr<StereoSGBM> leftmatcher = StereoSGBM::create(0, numDisparities, blockSize); // initialiser for the stereomatcher

boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer); // the pointcloud visualizer frame ie. namedWindow OCV

pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_value_ptr(new pcl::PointCloud<pcl::PointXYZ>); // create new global pointcloud for external access

float blue = 0;
float red = 0;
float green = 0;
static libusb_device_handle * dev_handle;

boost::shared_ptr<pcl::visualization::PCLVisualizer> updateViewer (pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloud)
{
// the callback function for the pointcloud visualizer, runs every refresh
    if (!runonce){
        viewer->setBackgroundColor (0, 0, 0);
        viewer->addPointCloud<pcl::PointXYZ> (cloud, "Stereo Camera");
        viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "Stereo Camera");
        viewer->addCoordinateSystem (1.0);
        viewer->initCameraParameters ();
        viewer->setCameraPosition(646.528, 1021.35, 1081.41, 292.365, 436.618, 413.724, -0.913514, 0.104311, 0.393207, 0); // start the camera over the data frame
        runonce = true;
    } else {
        viewer->updatePointCloud<pcl::PointXYZ>(cloud, "Stereo Camera");
    }
    return (viewer);
}

void takepicture(){ // copy both frames and output them to a file for debugging purposes

    filenum++;
    String leftstring;
    String rightstring;
    std::stringstream leftss;
    std::stringstream rightss;
    leftss << "\"VID5/left/xx" << filenum << ".jpg\"";
    rightss << "\"VID5/right/xx" << filenum << ".jpg\"";
    leftstring = leftss.str();
    rightstring = rightss.str();

    cout << leftstring << endl;
    cout << rightstring << endl;

    int retl = imwrite( leftstring, leftframe );
    if(retl<0){
        cout << "Left write failed!";
    }
    int retr = imwrite( rightstring, rightframe );
    if(retr<0){
        cout << "Right write failed!";
    }

}

void framefunction(uvc_frame_t *frame, void *ptr){

    uvc_frame_t *videoframe;
    uvc_error_t ret;

    videoframe = uvc_allocate_frame(frame->width * frame->height * 3);

    ret = uvc_mjpeg2rgb(frame, videoframe); // maybe convert to gray straight away to save a processing step? Depends on later obj detect stages
    if(ret){
        uvc_perror(ret, "uvc_mjpeg2rgb");
        uvc_free_frame(videoframe);
        return;
    }

    image.data = (uchar*)videoframe->data; // pull in the camera video frame from the device pointer

    flip(image,image,-1);

    image.colRange(0,640).copyTo(leftframe); // split the two unified frames into two separate ones
    image.colRange(640,1280).copyTo(rightframe);

    cvtColor(leftframe, leftframe, COLOR_RGB2BGR); // convert the rgb formatting into bgr for the compute function
    cvtColor(rightframe, rightframe, COLOR_RGB2BGR);

    if(video_filter){ // balance the video frames to make them more distinct
        equalizeHist(leftframe, leftframe);
        equalizeHist(rightframe, rightframe);
    }

    undistort(leftframe, leftcorrected, leftmatrix, leftdistcoeffs, leftdistmatrix); // correct the lens distortion using the generated coefficients
    undistort(rightframe, rightcorrected, rightmatrix, rightdistcoeffs, rightdistmatrix);
    Mat pcl_resize;

    leftcorrected.copyTo(pcl_overlay);


    char key = waitKey(1);
    if (key > 0) process_key_switches(key);
    uvc_free_frame(videoframe);

    return;
}

Mat process_transforms(){ // pull in values generated by Matlab and rectify the distortions in both lenses
    double rotmatrixval[9] = {0.999969171082056, 0.000687003787278125, -0.00782207844897380, -0.000537353386909107, 0.999817092838051, 0.0191178482106826, 0.00783378176892950, -0.0191130556077617, 0.999786638722748};
    double transvectorval[3] = {-57.9899512136651, -0.358844405843669, 0.353128133048344};

    Size framesize(640, 480);

    Mat R1; Mat R2; Mat P1; Mat P2; // most of these values have already been generated by OcamCalib or the Camera Calibrator (Matlab)
    Mat Q = cv::Mat(4, 4, CV_64F); // the only value we're interested in
    Mat rotmatrix = cv::Mat(3, 3, CV_64F, rotmatrixval);
    Mat transvector = cv::Mat(3, 1, CV_64F, transvectorval);

    Size newimageSize(640, 480); Rect validleftROI; Rect validrightROI;

    stereoRectify(leftmatrix, leftdistcoeffs, rightmatrix, rightdistcoeffs, framesize, rotmatrix, transvector, R1, R2, P1, P2, Q, CV_CALIB_ZERO_DISPARITY, 1, newimageSize, &validleftROI, &validrightROI);
    return Q;
}


int main(int argc, char **argv) {

    uvc_context_t *ctx; // setup UVC context
    uvc_device_t *camera; // setup instance of camera device
    uvc_device_handle_t *camerahandle; // setup device handle
    uvc_stream_ctrl_t ctrl; // setup control pointer
    uvc_error_t res; // setup error store

    try {

        res = uvc_init(&ctx, NULL); // initialise the UVC 'context'
        if (res < 0) {
            puts("Failed to initialise context.");
        }

        res = uvc_find_device(ctx, &camera, 0x1e4e, 0x0107, NULL); // find the camera based on it's vendor and product ID
        if (res < 0) {
            puts("Failed to find camera.");
        }

        res = uvc_open(camera, &camerahandle); // open the camera device
        if (res < 0) {
            puts("Failed to open the camera.");
        }

        dev_handle = uvc_get_libusb_handle(camerahandle);
        do_software_unlock();

        if(debug) {
            uvc_print_diag(camerahandle, stderr); // print device info
        }

        res = uvc_get_stream_ctrl_format_size(camerahandle, &ctrl, UVC_FRAME_FORMAT_MJPEG, 1280, 480,
                                              60); // negotiate an uncompressed stream 'profile'
        if (res < 0) {
            puts("Failed to negotiate stream profile.");
        }

        uvc_print_stream_ctrl(&ctrl, stderr); // print the result of the negotiation

        int value = 0;

        res = uvc_start_streaming(camerahandle, &ctrl, framefunction, (void *) (&value),
                                  0); // set streaming to use current handle and callback function
        if (res < 0) {
            puts("Failed to start stream."); // if the stream fails to initiate
            return -1;

        } else {

            uvc_set_ae_mode(camerahandle, 2); // set autoexposure on
            ledsoff();

            if(autoWB){
                enableAutoWhiteBalance();
            } else {
                disableAutoWhiteBalance();
            }

            uvc_set_ae_mode(camerahandle, 2);
            setColorGains(1.1, 1.1, 1.5);
            getColorGains(&red, &green, &blue);
            cout << "red: " << red << " green: " << green << " blue: " << blue << endl;

            struct timespec tim, tim2;
            tim.tv_sec = 0;
            tim.tv_nsec = 20;

            namedWindow("Depthmap", WINDOW_AUTOSIZE); // create the window pointers
            namedWindow("Left", WINDOW_AUTOSIZE);
            namedWindow("Left Corrected", WINDOW_AUTOSIZE);

            double lambda = 8000;
            double sigma = 1.2;
            Ptr<DisparityWLSFilter> wls_filter; // create the filter for the SGBM
            wls_filter = createDisparityWLSFilter(leftmatcher);
            wls_filter->setLambda(lambda);
            wls_filter->setSigmaColor(sigma);

            Mat leftdisp(480, 640, CV_32F, Scalar(0, 0, 0));
            Mat rightdisp(480, 640, CV_32F, Scalar(0, 0, 0));
            Mat filterdisp(480, 640, CV_32F, Scalar(0, 0, 0)); // CV_32FC4

            leftmatcher->setPreFilterCap(filtercap);
            leftmatcher->setP1(P1);
            leftmatcher->setP2(P2);
            leftmatcher->setDisp12MaxDiff(dispmaxdiff);
            leftmatcher->setSpeckleWindowSize(speckleWS);
            leftmatcher->setSpeckleRange(speckleR);
            leftmatcher->setMode(matchmode);

            Ptr<StereoMatcher> rightmatcher = createRightMatcher(leftmatcher);

            Mat filtered_disp_vis(480, 640, CV_32F, Scalar(0, 0, 0));
            Mat filtered_true_dmap(480, 640, CV_32F, Scalar(0, 0, 0));

            while (continue_processing) {

                // MAIN VARIABLES
                Mat leftframematch;
                leftcorrected.convertTo(leftframematch, CV_8U); // 8UC3
                Mat rightframematch;
                rightcorrected.convertTo(rightframematch, CV_8U); // 8UC3

                // MAIN PROCESS
                // compute the disparity map
                leftmatcher->compute(leftframematch, rightframematch, leftdisp);
                rightmatcher->compute(rightframematch, leftframematch, rightdisp);

                // post-filter disparity to smooth the output
                wls_filter->filter(leftdisp, leftframematch, filterdisp, rightdisp);

                // generate confidence map
                Mat conf_map = Mat(leftframe.rows, leftframe.cols, CV_32F);
                conf_map = wls_filter->getConfidenceMap();

                // visualise the disparity map
                getDisparityVis(filterdisp, filtered_disp_vis, 1.0);

                filtered_disp_vis.convertTo(filtered_true_dmap, CV_32F, 1.0 / 16.0, 0.0); // convert the depthmap into real values

                Mat trans_matrix = cv::Mat(4, 4, CV_32F);

                trans_matrix = process_transforms(); // retrieve the Q variable and pass to reproject
                reprojectImageTo3D(filtered_disp_vis, point_cloud_vals, trans_matrix, true, CV_32FC3); // reformat the data into a x, y, z-depth array per cell

                if (display_disparity) {
                    imshow("Depthmap", filtered_true_dmap);
                    imshow("Left", leftframe);
                    imshow("Left Corrected", leftcorrected);
                }

                if (display_pc){
                    cloud_value_ptr = visualiseCloud(point_cloud_vals); // call function to pass the depth map into the point cloud data format
                    viewer = updateViewer(cloud_value_ptr); // pass the pointer for the pointcloud data into the pcl viewer
                    viewer->spinOnce(1, false); // update the viewer ie. waitKey for OCV
                }
                char key = waitKey(1);
                if (key > 0) process_key_switches(key);
            }

            uvc_stop_streaming(camerahandle); // stop streaming and shutdown handle

        }
        destroyAllWindows();
        viewer->close();

        uvc_close(camerahandle); // release device handle
        uvc_unref_device(camera); // unreference the device instance
        uvc_exit(ctx); // close specific UVC context
    }

    catch( cv::Exception& e ){ // catch any exceptions within the 'try' loop and print them, whilst gracefully disconnecting the camera (doesn't work for functions outside the main loop)
        const char* err_msg = e.what();
        std::cout << "exception caught: " << err_msg << std::endl;
        cout << "Resetting USB device on exit!";
        uvc_close(camerahandle); // release device handle
        uvc_unref_device(camera); // unreference the device instance
        uvc_exit(ctx); // close specific UVC context
        libusb_reset_device(dev_handle);
    }
    return 0;
}

pcl::PointCloud<pcl::PointXYZ>::Ptr visualiseCloud(Mat cloud_mat){ // function that receives a Mat representing the depthmap image frame
    pcl::PointCloud<pcl::PointXYZ>::Ptr internal_value_ptr(new pcl::PointCloud<pcl::PointXYZ>); // create new pointer to a pointcloud containing cartesian and RGB data
    internal_value_ptr->width = (uint32_t) cloud_mat.cols; // dynamically set the frame size to that of the incoming frame
    internal_value_ptr->height = (uint32_t) cloud_mat.rows;
    for (int i = 0; i < cloud_mat.rows; i++) { // cycle through each row
        // cycle through every cell of that row
        for (int j = 0; j < cloud_mat.cols; j++) {
            // create a temporary point to pass out to the cloud
            pcl::PointXYZ cloud_point;
            cloud_point.x = i;
            cloud_point.y = j;
            cloud_point.z = (cloud_mat.at<cv::Vec3f>(i,j)[2]); // write the third cell of the vector (the depth) to the z axis of the points
            internal_value_ptr->points.push_back(cloud_point); //
        }

    }
    pcl::PassThrough<pcl::PointXYZ> pass; // 'pass through' filter, limits the points within a set range according to the z/depth data
    pass.setInputCloud(internal_value_ptr);
    pass.setFilterFieldName("z");
    pass.setFilterLimits(0, 40000); // -35000, 53000
    pass.setFilterLimitsNegative(false);
    pass.filter(*internal_value_ptr);
    return internal_value_ptr;
}

void process_key_switches(char key){ // bunch of random switches for certain options

    if (key == 27) continue_processing = false;
    if (key == 'd') no_distort = !no_distort;
    if (key == 'p') takepicture();
    if (key == 'c') display_confmap = !display_confmap;
    if (key == 's') display_disparity = !display_disparity;
    if (key == 'f') video_filter = !video_filter;
    if (key == 'r') show_raw_video = !show_raw_video;
    if (key == 't') process_transforms();
}



// WRITTEN BY COREY MANDERS FROM RACTIV - https://github.com/Ractiv/TouchPlus/tree/master/SDK/OS_X_linux/TouchPlus_viewer_and_lib_1.0

int do_software_unlock(){

    int r = 0;
    unsigned char bmRequestType_set = 0x21;
    unsigned char bmRequestType_get = 0xa1;
    unsigned char GET_CUR = 0x85;
    unsigned char SET_CUR = 0x01;
    unsigned short wValue =0x0300;
    unsigned short wIndex = 0x0400;
    unsigned char data[5];
    data[0] = 0x00;
    data[1] = 0x00;
    data[2] = 0x00;
    data[3] = 0x00;


    r= libusb_control_transfer(dev_handle,bmRequestType_get,GET_CUR,wValue,wIndex,data,2,10000 );

    data[0]=0x82;
    data[1]=0xf1;
    data[2]=0xf8;
    data[3]=0x00;

    r= libusb_control_transfer(dev_handle,bmRequestType_set,SET_CUR,wValue,wIndex,data,4,10000 );
    data[0]=0x00;
    data[1]=0x00;
    r= libusb_control_transfer(dev_handle,bmRequestType_get,GET_CUR,wValue,wIndex,data,2,10000 );

    data[0]=0x00;
    data[1]=0x00;
    data[2]=0x00;
    data[3]=0x00;
    GET_CUR = 0x81;
    r= libusb_control_transfer(dev_handle,bmRequestType_get,GET_CUR,wValue,wIndex,data,4,10000 );

    data[0]=0x00;
    data[1]=0x00;
    GET_CUR=0x85;
    r= libusb_control_transfer(dev_handle,bmRequestType_get,GET_CUR,wValue,wIndex,data,2,10000);

    data[0]=0x02;
    data[1]=0xf1;
    data[2]=0xf8;
    data[3]=0x40;
    r= libusb_control_transfer(dev_handle,bmRequestType_set,SET_CUR,wValue,wIndex,data,4,0 );

    data[0]=0x00;
    data[1]=0x00;
    r= libusb_control_transfer(dev_handle,bmRequestType_get,GET_CUR,wValue,wIndex,data,2,0 );

    data[0]=0x00;
    data[1]=0x00;
    data[2]=0x00;
    data[3]=0x00;
    GET_CUR=0x81;
    r= libusb_control_transfer(dev_handle,bmRequestType_get,GET_CUR,wValue,wIndex,data,4,0 );

    return 0;
}

int ledsoff()
{

    unsigned char bmRequestType_set = 0x21;
    unsigned char bmRequestType_get = 0xa1;
    unsigned short wValue =0x0300;
    unsigned short wIndex = 0x0400;
    unsigned char data2[2];
    unsigned char data4[4];

    libusb_control_transfer(dev_handle, bmRequestType_get, 0x85, wValue, wIndex, data2, 2, 10000);
    data4[0]=0x82;
    data4[1]=0xf0;
    data4[2]=0x17;
    data4[3]=0x00;
    libusb_control_transfer(dev_handle, bmRequestType_set, 0x01, wValue, wIndex, data4, 4, 10000);

    libusb_control_transfer(dev_handle, bmRequestType_get, 0x85, wValue, wIndex, data2, 2, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x81, wValue, wIndex, data4, 4, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x85, wValue, wIndex, data2, 2, 10000);

    data4[0]=0x02;
    data4[1]=0xf0;
    data4[2]=0x17;
    data4[3]=0x15;

    libusb_control_transfer(dev_handle, bmRequestType_set, 0x01, wValue, wIndex, data4, 4, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x85, wValue, wIndex, data2, 2, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x81, wValue, wIndex, data4, 4, 10000);

    return 0;
}

int getColorGains(float *red, float *green, float *blue){

    unsigned char bmRequestType_set = 0x21;
    unsigned char bmRequestType_get = 0xa1;
    unsigned short wValue =0x0300;
    unsigned short wIndex = 0x0400;
    unsigned char data2[2];
    unsigned char data4[4];
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x85, wValue, wIndex, data2, 2, 10000);

    data4[0] = 0x82;
    data4[1] = 0xf1;
    data4[2] = 0xcc;
    data4[3] = 0X00;
    libusb_control_transfer(dev_handle, bmRequestType_set, 0x01, wValue, wIndex, data4, 4, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x85, wValue, wIndex, data2, 2, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x81, wValue, wIndex, data4, 4, 10000);
    float mred=(float)data4[1];
    *red = mred/64;


    //  printf("red data = %x\n",data[1]);

    libusb_control_transfer(dev_handle, bmRequestType_get, 0x85, wValue, wIndex, data2, 2, 10000);
    data4[0] = 0x82;
    data4[1] = 0xf1;
    data4[2] = 0xcd;
    data4[3] = 0X00;
    libusb_control_transfer(dev_handle, bmRequestType_set, 0x01, wValue, wIndex, data4, 4, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x85, wValue, wIndex, data2, 2, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x81, wValue, wIndex, data4, 4, 10000);
    //  printf("green data = %x\n",data[1]);
    float mgreen= (float)data4[1];
    *green = mgreen/64;

    libusb_control_transfer(dev_handle, bmRequestType_get, 0x85, wValue, wIndex, data2, 2, 10000);
    data4[0] = 0x82;
    data4[1] = 0xf1;
    data4[2] = 0xcf;
    data4[3] = 0X00;
    libusb_control_transfer(dev_handle, bmRequestType_set, 0x01, wValue, wIndex, data4, 4, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x85, wValue, wIndex, data2, 2, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x81, wValue, wIndex, data4, 4, 10000);
    //   printf("blue data = %x\n",data[1]);
    float mblue = (float)data4[1];
    *blue = mblue/64;
    return 0;
}

int enableAutoWhiteBalance() {

    unsigned char bmRequestType_set = 0x21;
    unsigned char bmRequestType_get = 0xa1;
    unsigned char GET_CUR = 0x85;
    unsigned char SET_CUR = 0x01;
    unsigned short wValue = 0x0300;
    unsigned short wIndex = 0x0400;
    unsigned char data[5];
    data[0] = 0x00;
    data[1] = 0x00;
    data[2] = 0x00;
    data[3] = 0x00;

    int r = libusb_control_transfer(dev_handle, bmRequestType_get, GET_CUR, wValue, wIndex, data, 2, 10000);
    //cout << "get returned " << r << endl;
    //for (int i = 0; i < 2; i++) {
    //    printf("data[%d] = %x\n", i, data[i]);
    //}
    data[0] = 0x20;
    data[1] = 0xa8;
    data[2] = 0x00;
    data[3] = 0x00;
    r = libusb_control_transfer(dev_handle, bmRequestType_set, SET_CUR, wValue, wIndex, data, 4, 10000);
    //cout << "set returned " << r << endl;
    //for (int i = 0; i < 4; i++) {
    //    printf("data[%d] = %x\n", i, data[i]);
    //}

    r = libusb_control_transfer(dev_handle, bmRequestType_get, GET_CUR, wValue, wIndex, data, 2, 10000);
    //cout << "get returned " << r << endl;
    //for (int i = 0; i < 2; i++) {
    //    printf("data[%d] = %x\n", i, data[i]);
    //}
    GET_CUR = 0x81;
    r = libusb_control_transfer(dev_handle, bmRequestType_get, GET_CUR, wValue, wIndex, data, 4, 10000);
    //cout << "get returned " << r << endl;
    //for (int i = 0; i < 4; i++) {
    //    printf("data[%d] = %x\n", i, data[i]);
    //}
    GET_CUR = 0x85;
    r = libusb_control_transfer(dev_handle, bmRequestType_get, GET_CUR, wValue, wIndex, data, 2, 10000);
    //cout << "get returned " << r << endl;
    //for (int i = 0; i < 2; i++) {
    //    printf("data[%d] = %x\n", i, data[i]);
    //}

    data[0] = 0x20;
    data[1] = 0xa9;
    data[2] = 0x0C;
    data[3] = 0x00;
    r = libusb_control_transfer(dev_handle, bmRequestType_set, SET_CUR, wValue, wIndex, data, 4, 10000);
    //cout << "set returned " << r << endl;
    //for (int i = 0; i < 4; i++) {
    //    printf("data[%d] = %x\n", i, data[i]);
    //}
    r = libusb_control_transfer(dev_handle, bmRequestType_get, GET_CUR, wValue, wIndex, data, 2, 10000);
    //cout << "get returned " << r << endl;
    //for (int i = 0; i < 2; i++) {
    //    printf("data[%d] = %x\n", i, data[i]);
    //}
    GET_CUR = 0x81;
    r = libusb_control_transfer(dev_handle, bmRequestType_get, GET_CUR, wValue, wIndex, data, 4, 10000);
    //cout << "get returned " << r << endl;
    //for (int i = 0; i < 4; i++) {
    //    printf("data[%d] = %x\n", i, data[i]);
    //}
    GET_CUR = 0x85;
    r = libusb_control_transfer(dev_handle, bmRequestType_get, GET_CUR, wValue, wIndex, data, 2, 10000);
    //cout << "get returned " << r << endl;
    //for (int i = 0; i < 2; i++) {
    //    printf("data[%d] = %x\n", i, data[i]);
    //}
    data[0] = 0x20;
    data[1] = 0xaa;
    data[2] = 0x01;
    data[3] = 0x00;
    r = libusb_control_transfer(dev_handle, bmRequestType_set, SET_CUR, wValue, wIndex, data, 4, 10000);
    //cout << "set returned " << r << endl;
    //for (int i = 0; i < 4; i++) {
    //    printf("data[%d] = %x\n", i, data[i]);
    //}
    GET_CUR = 0x85;
    r = libusb_control_transfer(dev_handle, bmRequestType_get, GET_CUR, wValue, wIndex, data, 2, 10000);
    //cout << "get returned " << r << endl;
    //for (int i = 0; i < 2; i++) {
    //    printf("data[%d] = %x\n", i, data[i]);
    //}
    GET_CUR = 0x81;
    r = libusb_control_transfer(dev_handle, bmRequestType_get, GET_CUR, wValue, wIndex, data, 4, 10000);
    //cout << "get returned " << r << endl;
    //for (int i = 0; i < 4; i++) {
    //    printf("data[%d] = %x\n", i, data[i]);
    //}

    return 0;

}

int setColorGains(float red, float green, float blue){

    unsigned char bmRequestType_set = 0x21;
    unsigned char bmRequestType_get = 0xa1;
    unsigned char GET_CUR = 0x85;
    unsigned char SET_CUR = 0x01;
    unsigned short wValue = 0x0300;
    unsigned short wIndex = 0x0400;
    unsigned char data4[4];
    unsigned char data2[2];
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x85, wValue, wIndex, data2, 2, 10000);

    //set red
    int toSet = (red *64);
    if (toSet<0)toSet =0;
    if (toSet>255)toSet = 255;
    data4[0] = 0x02;
    data4[1] = 0xf1;
    data4[2] = 0xcc;
    data4[3] = (unsigned char)toSet;
    libusb_control_transfer(dev_handle, bmRequestType_set, 0x01, wValue, wIndex, data4, 4, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x85, wValue, wIndex, data2, 2, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x81, wValue, wIndex, data4, 4, 10000);

    data4[0] = 0x02;
    data4[1] = 0xf2;
    data4[2] = 0x8c;
    data4[3] = (unsigned char)toSet;
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x85, wValue, wIndex, data2, 2, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_set, 0x01, wValue, wIndex, data4, 4, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x85, wValue, wIndex, data2, 2, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x81, wValue, wIndex, data4, 4, 10000);



    /// green
    int toSet2 = (green *64);
    if (toSet2<0)toSet2 =0;
    if (toSet2>255)toSet2 = 255;
    data4[0] = 0x02;
    data4[1] = 0xf1;
    data4[2] = 0xcd;
    data4[3] = (unsigned char)toSet2;
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x85, wValue, wIndex, data2, 2, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_set, 0x01, wValue, wIndex, data4, 4, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x85, wValue, wIndex, data2, 2, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x81, wValue, wIndex, data4, 4, 10000);

    data4[0] = 0x02;
    data4[1] = 0xf2;
    data4[2] = 0x8d;
    data4[3] = (unsigned char)toSet2;
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x85, wValue, wIndex, data2, 2, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_set, 0x01, wValue, wIndex, data4, 4, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x85, wValue, wIndex, data2, 2, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x81, wValue, wIndex, data4, 4, 10000);

    data4[0] = 0x02;
    data4[1] = 0xf1;
    data4[2] = 0xce;
    data4[3] = (unsigned char)toSet2;
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x85, wValue, wIndex, data2, 2, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_set, 0x01, wValue, wIndex, data4, 4, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x85, wValue, wIndex, data2, 2, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x81, wValue, wIndex, data4, 4, 10000);

    data4[0] = 0x02;
    data4[1] = 0xf2;
    data4[2] = 0x8e;
    data4[3] = (unsigned char)toSet2;
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x85, wValue, wIndex, data2, 2, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_set, 0x01, wValue, wIndex, data4, 4, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x85, wValue, wIndex, data2, 2, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x81, wValue, wIndex, data4, 4, 10000);




    toSet2 = (green *64);
    if (toSet2<0)toSet2 =0;
    if (toSet2>255)toSet2 = 255;
    data4[0] = 0x02;
    data4[1] = 0xf1;
    data4[2] = 0xce;
    data4[3] = (unsigned char)toSet2;
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x85, wValue, wIndex, data2, 2, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_set, 0x01, wValue, wIndex, data4, 4, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x85, wValue, wIndex, data2, 2, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x81, wValue, wIndex, data4, 4, 10000);


    //blue side 1
    int toSet3 = (blue *64);
    if (toSet3<0)toSet3 =0;
    if (toSet3>255)toSet3 = 255;
    data4[0] = 0x02;
    data4[1] = 0xf1;
    data4[2] = 0xcf;
    data4[3] = (unsigned char)toSet3;
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x85, wValue, wIndex, data2, 2, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_set, 0x01, wValue, wIndex, data4, 4, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x85, wValue, wIndex, data2, 2, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x81, wValue, wIndex, data4, 4, 10000);

    data4[0] = 0x02;
    data4[1] = 0xf2;
    data4[2] = 0x8f;
    data4[3] = (unsigned char)toSet3;
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x85, wValue, wIndex, data2, 2, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_set, 0x01, wValue, wIndex, data4, 4, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x85, wValue, wIndex, data2, 2, 10000);
    libusb_control_transfer(dev_handle, bmRequestType_get, 0x81, wValue, wIndex, data4, 4, 10000);

    return 0;
}

int disableAutoWhiteBalance(){

    unsigned char bmRequestType_set = 0x21;
    unsigned char bmRequestType_get = 0xa1;
    unsigned char GET_CUR = 0x85;
    unsigned char SET_CUR = 0x01;
    unsigned short wValue =0x0300;
    unsigned short wIndex = 0x0400;
    unsigned char data[5];
    data[0] = 0x00;
    data[1] = 0x00;
    data[2] = 0x00;
    data[3] = 0x00;

    int r= libusb_control_transfer(dev_handle,bmRequestType_get,GET_CUR,wValue,wIndex,data,2,10000 );
    cout <<"get returned "<<r<<endl;
    for (int i = 0;i<2; i++){
        printf("data[%d] = %x\n",i,data[i]);
    }
    data[0] = 0x20;
    data[1] = 0xa8;
    data[2] = 0x00;
    data[3] = 0x00;
    r= libusb_control_transfer(dev_handle,bmRequestType_set,SET_CUR,wValue,wIndex,data,4,10000 );
    cout <<"set returned "<<r<<endl;
    for (int i = 0;i<4; i++){
        printf("data[%d] = %x\n",i,data[i]);
    }

    r= libusb_control_transfer(dev_handle,bmRequestType_get,GET_CUR,wValue,wIndex,data,2,10000 );
    cout <<"get returned "<<r<<endl;
    for (int i = 0;i<2; i++){
        printf("data[%d] = %x\n",i,data[i]);
    }
    GET_CUR = 0x81;
    r= libusb_control_transfer(dev_handle,bmRequestType_get,GET_CUR,wValue,wIndex,data,4,10000 );
    cout <<"get returned "<<r<<endl;
    for (int i = 0;i<4; i++){
        printf("data[%d] = %x\n",i,data[i]);
    }
    GET_CUR = 0x85;
    r= libusb_control_transfer(dev_handle,bmRequestType_get,GET_CUR,wValue,wIndex,data,2,10000 );
    cout <<"get returned "<<r<<endl;
    for (int i = 0;i<2; i++){
        printf("data[%d] = %x\n",i,data[i]);
    }

    data[0] = 0x20;
    data[1] = 0xa9;
    data[2] = 0x0C;
    data[3] = 0x00;
    r= libusb_control_transfer(dev_handle,bmRequestType_set,SET_CUR,wValue,wIndex,data,4,10000 );
    cout <<"set returned "<<r<<endl;
    for (int i = 0;i<4; i++){
        printf("data[%d] = %x\n",i,data[i]);
    }
    r= libusb_control_transfer(dev_handle,bmRequestType_get,GET_CUR,wValue,wIndex,data,2,10000 );
    cout <<"get returned "<<r<<endl;
    for (int i = 0;i<2; i++){
        printf("data[%d] = %x\n",i,data[i]);
    }
    GET_CUR = 0x81;
    r= libusb_control_transfer(dev_handle,bmRequestType_get,GET_CUR,wValue,wIndex,data,4,10000 );
    cout <<"get returned "<<r<<endl;
    for (int i = 0;i<4; i++){
        printf("data[%d] = %x\n",i,data[i]);
    }
    GET_CUR = 0x85;
    r= libusb_control_transfer(dev_handle,bmRequestType_get,GET_CUR,wValue,wIndex,data,2,10000 );
    cout <<"get returned "<<r<<endl;
    for (int i = 0;i<2; i++){
        printf("data[%d] = %x\n",i,data[i]);
    }
    data[0] = 0x20;
    data[1] = 0xaa;
    data[2] = 0x00;
    data[3] = 0x00;
    r= libusb_control_transfer(dev_handle,bmRequestType_set,SET_CUR,wValue,wIndex,data,4,10000 );
    cout <<"set returned "<<r<<endl;
    for (int i = 0;i<4; i++){
        printf("data[%d] = %x\n",i,data[i]);
    }
    GET_CUR = 0x85;
    r= libusb_control_transfer(dev_handle,bmRequestType_get,GET_CUR,wValue,wIndex,data,2,10000 );
    cout <<"get returned "<<r<<endl;
    for (int i = 0;i<2; i++){
        printf("data[%d] = %x\n",i,data[i]);
    }
    GET_CUR = 0x81;
    r= libusb_control_transfer(dev_handle,bmRequestType_get,GET_CUR,wValue,wIndex,data,4,10000 );
    cout <<"get returned "<<r<<endl;
    for (int i = 0;i<4; i++){
        printf("data[%d] = %x\n",i,data[i]);
    }


    return 0;

}

// END OF SECTION WRITTEN BY COREY MANDERS FROM RACTIV