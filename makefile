CPPFLAGS = $(shell pkg-config --cflags opencv)
LDLIBS = $(shell pkg-config --libs opencv)

all: test TouchPlus

TouchPlus: TouchPlus.cpp TouchPlus.h
	g++ -I/usr/local/include/libusb-1.0 -c TouchPlus.cpp

test: test.cpp TouchPlus.o
	g++ test.cpp TouchPlus.o -I/usr/local/include/libusb-1.0 -o test `pkg-config opencv --libs` -L./ -luvc -lusb-1.0 -lopencv_ximgproc

depthmap: TouchPlus depthmap.cpp
	g++ depthmap.cpp TouchPlus.o -o depthmap $(LDLIBS) $(CPPFLAGS) -I/usr/local/include/libusb-1.0 -luvc -lusb-1.0

calibrate: calibrate.cpp
	g++ -c calibrate.cpp -o calibrate $(LDLIBS) $(CPPFLAGS)

clean:
	rm test TouchPlus.o depthmap
