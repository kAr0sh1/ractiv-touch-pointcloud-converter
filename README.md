# README #

This is a repo to chart and secure the work conducted on the AINT513 Visual Perception coursework undertaken as part of an MSc in Robotics.

### How do I get set up? ###

To run the original setup a Ractiv Touch+ camera is required, however any UVC stereo camera can be substituted as long as the calibration parameters are changed.

### Dependencies ###

OpenCV, PCL and CMake.

### Deployment instructions ###

Program is tested on OSX only, written in CLion which uses CMake to build. CMake and a clearly setup path to the required libraries should be the only things necessary for deployment.

### Contribution guidelines ###

This is a single student project with no collaboration from the outside allowed. Outside access to this repo will be granted purely for assessment purposes.

### Who do I talk to? ###

* Repo owner or admin

kAr0sh1