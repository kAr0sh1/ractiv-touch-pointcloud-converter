Mat calibimage; // calibration matrix for fisheye correction
patternSize = cvSize(5,7); // size of chessboard pattern in columns and rows

// take in settings file for the filenames etc
findChessboardCorners(calibimage, patternSize, imagecorners, CV_CALIB_CB_ADAPTIVE_THRESH+CV_CALIB_CB_FILTER_QUADS);

// take in object points and output the image points
// also output jacobian matrix of derivatives of image points
fisheye::projectPoints();

// take in object points and both image points sets
// output rotation matrix and translation vector between cameras
// also output K1, D1, K2 and D2 coefficients
fisheye::stereoCalibrate();

// take in coefficients, rotation matrix and translation vector
// output R1, R2, P1 and P2 rectification transform and project matrices
fisheye::stereoRectify();

// take in image and camera matric and distortion coefficients
// output corrected image
fisheye::undistortImage();
