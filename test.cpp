#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp> 
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/ximgproc/disparity_filter.hpp>
#include <iostream>
#include <string>
#include <stdio.h>
#include <unistd.h>
#include "TouchPlus.h"


#define MJPEG 1
#define UNCOMPRESSED 0

#define WIDTH 1280
#define HEIGHT 480
#define FRAMERATE 60
#define COMPRESSION MJPEG

using namespace cv;
using namespace cv::ximgproc;
using namespace std;

Mat image(HEIGHT, WIDTH, CV_8UC3, Scalar(0, 0, 0));
Mat left_for_matcher(480, 640, CV_8UC3, Scalar(0, 0, 0));
Mat right_for_matcher(480, 640, CV_8UC3, Scalar(0, 0, 0));
Mat leftframe(480, 640, CV_8UC3, Scalar(0, 0, 0));
Mat rightframe(480, 640, CV_8UC3, Scalar(0, 0, 0));
Mat left_disp,right_disp;
Mat filtered_disp;
Mat disparity(480, 640, CV_16S, Scalar(0, 0, 0));
Mat imageoutput(480, 640, CV_8UC1, Scalar(0, 0, 0));


bool leds_on = false;
bool use_ae = false;
bool use_awb = false;

volatile bool continue_processing = true;

unsigned char *buffer;
int decompressJPEG = 0;
uvc_frame_t *bgr;
unsigned char * liveData;
volatile long currentFrame = 0;
int mode = 0;
int blocksize = 15;
int max_disp = 16;
int wsize = 3;
double lambda = 8000.0;
double sigma  = 1.5;
double vis_mult = 1.0;
double matching_time, filtering_time;

Rect computeROI(Size2i src_sz, Ptr<StereoMatcher> matcher_instance);

void process_key_switches(char key){

  if (key == 27) continue_processing = false;

}

void cb(uvc_frame_t *frame, void *ptr) {

  	uvc_error_t ret;
  	Rect ROI;
  	Ptr<DisparityWLSFilter> wls_filter;

  if (COMPRESSION == UNCOMPRESSED){
    ret = uvc_any2bgr(frame, bgr);
  }
  else {
    ret = uvc_mjpeg2rgb(frame, bgr);

  if (ret) {
    uvc_perror(ret, "change to BGR error");
    uvc_free_frame(bgr);
    return;
  }
    unsigned char tmp;

    image.data = (uchar*)bgr->data;
    flip(image,image,0);

    image.colRange(0,640).copyTo(leftframe);
    image.colRange(640,1280).copyTo(rightframe);
    
    Ptr<StereoSGBM> matcher = StereoSGBM::create(0,max_disp,wsize);
    matcher->setUniquenessRatio(0);
    matcher->setDisp12MaxDiff(1000000);
    matcher->setSpeckleWindowSize(0);
    matcher->setP1(24*wsize*wsize);
    matcher->setP2(96*wsize*wsize);
    matcher->setMode(StereoSGBM::MODE_SGBM_3WAY);
    ROI = computeROI(left_for_matcher.size(),matcher);
    wls_filter = createDisparityWLSFilterGeneric(false);
    wls_filter->setDepthDiscontinuityRadius((int)ceil(0.5*wsize));

    matching_time = (double)getTickCount();
    matcher->compute(left_for_matcher,right_for_matcher,left_disp);
    matching_time = ((double)getTickCount() - matching_time)/getTickFrequency();	
	// visualise
	wls_filter->setLambda(lambda);
    wls_filter->setSigmaColor(sigma);
	filtering_time = (double)getTickCount();
    wls_filter->filter(left_disp,leftframe,filtered_disp,Mat(),ROI);
    filtering_time = ((double)getTickCount() - filtering_time)/getTickFrequency();
    
    //imshow("Left Image", leftframe);
    //imshow("Depth Map", filtered_disp);
    //imshow("Right Image", rightframe);
    char key = waitKey(5);

  }
}

int main(int ac, char** av)
{

    bgr = uvc_allocate_frame(WIDTH*HEIGHT*3);

    fprintf(stderr,"about to start streaming\n");

    init_camera();

    int retval = startVideoStream(WIDTH,HEIGHT,60,MJPEG,cb);
    if (retval == -1){
    	return -1;
    }
    turnLEDsOff();
    int x,y,z;
    getAccelerometerValues(&x,&y,&z);
    fprintf(stderr,"x= %d y= %d z= %d\n",x,y,z);


    while(continue_processing){
       	Mat raw_disp_vis;
    	getDisparityVis(left_disp,raw_disp_vis,vis_mult);
        namedWindow("raw disparity", WINDOW_AUTOSIZE);
        imshow("raw disparity", raw_disp_vis);
        Mat filtered_disp_vis;
        getDisparityVis(filtered_disp,filtered_disp_vis,vis_mult);
        namedWindow("filtered disparity", WINDOW_AUTOSIZE);
        imshow("filtered disparity", filtered_disp_vis);
        waitKey();
    }
    destroyWindow("raw disparity");
    destroyWindow("filtered disparity");
    stop_VideoStream();

}

Rect computeROI(Size2i src_sz, Ptr<StereoMatcher> matcher_instance)
{
    int min_disparity = matcher_instance->getMinDisparity();
    int num_disparities = matcher_instance->getNumDisparities();
    int block_size = matcher_instance->getBlockSize();

    int bs2 = block_size/2;
    int minD = min_disparity, maxD = min_disparity + num_disparities - 1;

    int xmin = maxD + bs2;
    int xmax = src_sz.width + minD - bs2;
    int ymin = bs2;
    int ymax = src_sz.height - bs2;

    Rect r(xmin, ymin, xmax - xmin, ymax - ymin);
    return r;
}
