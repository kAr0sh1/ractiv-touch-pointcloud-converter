//
// Created by Louis King on 07/04/2016.
//
#include <libuvc/libuvc.h>
#include <opencv2/stereo/stereo.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/photo.hpp>
#include <opencv2/stitching.hpp>
#include <opencv2/ximgproc/disparity_filter.hpp>

#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/console/parse.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/passthrough.h>
#include <Eigen/Dense>

#ifndef PCLMAPPER_DEPTHMAP_PCL_HPP_H
#define PCLMAPPER_DEPTHMAP_PCL_HPP_H


void takepicture();
void process_key_switches(char key);
pcl::PointCloud<pcl::PointXYZ>::Ptr visualiseCloud(cv::Mat cloud_mat);


// WRITTEN BY COREY MANDERS FROM RACTIV - https://github.com/Ractiv/TouchPlus/tree/master/SDK/OS_X_linux/TouchPlus_viewer_and_lib_1.0

int do_software_unlock();
int ledsoff();
int enableAutoWhiteBalance();
int getColorGains(float *red, float *green, float *blue);
int setColorGains(float red, float green, float blue);
int disableAutoWhiteBalance();

// END OF SECTION WRITTEN BY COREY MANDERS FROM RACTIV


#endif //PCLMAPPER_DEPTHMAP_PCL_HPP_H
